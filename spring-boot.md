# Introduction
The Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications. The 
main features of the Spring Boot:

* Create stand-alone Spring applications
* Embed Tomcat, Jetty or Undertow directly (no need to deploy WAR files)
* Provide opinionated 'starter' dependencies to simplify your build configuration
* Automatically configure Spring and 3rd party libraries whenever possible
* Provide production-ready features such as metrics, health checks and externalized configuration
* Absolutely no code generation and no requirement for XML configuration

# Spring Boot Labs
## General concept

1.. The best is to use the [Spring initializer](https://start.spring.io/) website, where you can define the Spring Boot 
   starters, you want to use in your application. So, open the website and add following Dependencies: _Web, JPA,_ and _H2_.
   Type Group as _ite_ and Artifact as _librarymaster_ (or you can choose your Group and Artifact). Then choose _Gradle_ project 
   (or Maven, if Maven is your you preferred build tool), **Java** and the latest stable Spring boot version. Now click the **Generate Project** button and
   save resulting zip file. Now unzip downloaded archive and import project into your favorite Java IDE. 
   The same you can do from the Spring Tool Suite IDE provided by Pivotal.
   
2.. Open the generated entry Java class, which contains the application bootstrap code.

```java
    @SpringBootApplication
    public class Application {

        public static void main(String[] args) {
          SpringApplication.run(Application.class, args);
        }

    }
```

	  
   
   The @SpringBootApplication annotation triggers the Autoconfiguration (will be introduced later) and Component scanning.
   If you like to list all configured bean names, add following code into main():
   
```java
   ApplicationContext ctx = SpringApplication.run(App.class, args);
         
       String[] beanNames = ctx.getBeanDefinitionNames();
         
       Arrays.sort(beanNames);
 
       for (String beanName : beanNames) {
           System.out.println(beanName);
       }
```   


3.. The Spring Boot introduces starters, which are templates that contain a collection of all the relevant transitive dependencies that are needed to start a particular functionality. For instance the _spring-boot-starter-web_ brings all required dependencies to your project. The list of dependencies for this starter is [here](https://github.com/spring-projects/spring-boot/blob/master/spring-boot-project/spring-boot-starters/spring-boot-starter-web/build.gradle).
This feature of Spring Boot introduces great simplification of the project setup, as dependency management was becoming very complex task recently.

* [List of Spring Boot starters](https://github.com/spring-projects/spring-boot/tree/master/spring-boot-project/spring-boot-starters/)
* [Creating Your Own Starter](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-developing-auto-configuration.html#boot-features-custom-starter)
   
4.. The Spring Boot [Auto-configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-auto-configuration.html) scans the classpath, finds the libraries in the classpath and then attempt to guess the best configuration for them, and finally configure all such beans. Autoconfiguration is enabled with @EnableAutoConfiguration annotation (already included as part of @SpringBootApplication annotation). You can override auto-configured components by your custom configuration.  Check the _spring-boot-autoconfigure-<SpringBoot version>.jar_.  There is for instance the _org.springframework.boot.autoconfigure.jdbc_, which contains set of Classes, which attempt to configure datasource.
   
* [Your own autoconfiguration](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-developing-auto-configuration.html)
   
5.. The web starter includes also dependency on embedded Tomcat AS starter. If you like to use different supported Servlet container, you can exclude
   the Tomcat stater and include for example the Jetty starter:
   
```
   dependencies {
     implementation("org.springframework.boot:spring-boot-starter-web") {
       exclude module: "spring-boot-starter-tomcat"
     }
     implementation("org.springframework.boot:spring-boot-starter-jetty")
   }
``` 
   
6.. The configuration of Boot application can be defined in _application.properties|yml_ file. For example, if you want to start application on different
   port and some context, use following properties:
   
```
   server.port=8081
   server.contextPath=/myapp
   
```     
   
7. By default, the built artifact is the jar file, which contains all required dependencies and can be started as simple as `java -jar my-app.jar`.
   Spring Boot Gradle/Maven plug-in defines tasg/target _bootRun_, which can be used to start application as well.  
   
8. You can deploy application as standard WAR archive. Just include war plugin in Gradle and exclude the tomcat starter from dependency configuration.

```
   implementation('org.springframework.boot:spring-boot-starter-web'){
     exclude module: "spring-boot-starter-tomcat"
   }
```

## Logging     
   
1.. Logging in spring boot is very flexible and easy to configure. Spring boot supports various logging providers (Logback, JUL, Lo4J2).
   The default provider is Logback and the Console logger is auto-configured.
   
2.. The Spring Boot uses the SLf4J logging wrapper. To get logger use:

```java
    private static final Logger LOGGER=LoggerFactory.getLogger(Application.class);
```

3.. Global Logging level can be configured in application.properties/yaml or as application parameter.

```
# In properties file
debug=true
 
# In Console
$ java -jar my-app.jar --trace
``` 

4.. Single logger level can be configured as well:

```
# In Console
-Dlogging.level.org.springframework=ERROR
-Dlogging.level.ite.librarymaster=TRACE
 
# In properties file
logging.level.org.springframework=ERROR
logging.level.ite.librarymaster=TRACE

```

5.. Logging pattern can be configured as well:

```
logging.file=application.log
# Logging pattern for the console
logging.pattern.console= %d{yyyy-MM-dd HH:mm:ss} - %logger{36} - %msg%n
  
# Logging pattern for file
logging.pattern.file= %d{yyyy-MM-dd HH:mm:ss} [%thread] %-5level %logger{36} - %msg%
```

You can use also the logging configuration file. For example _logback.xml_. 

## Testing
In your tests, you can use:

1. @RunWith(SpringRunner.class)
2. @RunWith(MockitoJUnitRunner.class) - if you will use Mocking by the Mockito tool.
3. @SpringBootTest - starts the embedded server and fully initializes the application context. 
You can also limit Application Context only to a set of spring components that participate in test scenario.

## CommandLineRunner
It can be used to run a code block after application is initialized.

```java
    public class Application implements CommandLineRunner{...}
```

Command Line Runner is called before execution of the main() method is complete.  



        
    
    
    
    
   

